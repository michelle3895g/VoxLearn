'''Parsing script, setup as an importable class'''
import re
pattern = re.compile('^ATOM(.)*|^HETATM(.)*')
sdf_descriptor = re.compile('^(.)*V2000')

def pdb_parser(file_name):
    '''
    Accepts:
    file_name : the name of a PDB file to parse
    Returns :
    List of lists w/ atoms in format...
    '''
    atom_data = []

    file_handle = open(file_name, 'r', encoding='UTF-8')
    for line in file_handle:
        if pattern.search(line):
            residue_name = line[17:20].strip()
            chain = line[21].strip()
            residue_number = int(line[22:26].strip())
            insertion_code = line[26].strip()
            x_coord = float(line[30:38].strip())
            y_coord = float(line[38:46].strip())
            z_coord = float(line[46:54].strip())
            element = line[76:78].strip()
            charge = line[78:90].strip()
            this_atom = [x_coord, y_coord, z_coord, element, charge, residue_name,
                        chain, residue_number, insertion_code]
            atom_data.append(this_atom)

    file_handle.close()
    return atom_data

def sdf_parser(file_name):
    '''
    Accepts:
    file_name : the name of a SDF file to parse
    Returns :
    List of lists w/ atoms in format...

    read counts line (only V2000 currently) first int is the number of atoms, n
    start at counts line, iterate through n lines, if empty don't count
    then read next n non-blank lines. coordinates are 10 characters wide each, then space, then three characters for atomic symbol
    '''
    atom_data = []
    file_info = []
    charge_dict = {0:0,1:3,2:2,3:1,4:4,5:-1,6:-2,7:-3}
    #open file
    file_handle = open(file_name, 'r', encoding='UTF-8')
    #find sdf/mol file counts line
    for line in file_handle:
        if sdf_descriptor.search(line):
            file_info.append(line)
            number_atoms = int(file_info[0][:3])
            current_count = 0
            while current_count < number_atoms:
                line = next(file_handle)
                x_coord = float(line[0:10].strip())
                y_coord = float(line[10:20].strip())
                z_coord = float(line[20:30].strip())
                element = line[31:34].strip()
                charge = int(line[36:39].strip())
                charge = charge_dict[charge]
                this_atom = [x_coord, y_coord, z_coord, element, charge]
                atom_data.append(this_atom)
                if charge == 4:
                    raise ValueError('Found doublet radical!')
                if line.strip():
                    current_count += 1
                
    file_handle.close()

    #if more than one or zero counts lines, break
    if len(file_info) != 1:
        raise ValueError('Found wrong number of SDF/MOL file counts lines - expected one')
    
    return atom_data

if __name__ == "__main__":
    main()
