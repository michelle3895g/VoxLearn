FROM 

# System maintenance
RUN apt update && apt-get install -y python3-tk
RUN pip install --upgrade pip

# Set working directory
WORKDIR /project

# clone project
RUN git clone https://www.gitlab.io/ajaceves/molecule_completion

CMD bash
