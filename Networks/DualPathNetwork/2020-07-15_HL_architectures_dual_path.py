#Import built in libraries
import glob, os, csv, random

#Third party import
import argparse
import numpy as np
import sklearn
from sklearn.preprocessing import MinMaxScaler

#Third party imports from keras
from tensorflow import keras
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv3D, MaxPool3D, BatchNormalization, Input, AveragePooling3D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Model

#hyperparameter tuning
from tensorboard.plugins.hparams import api as hp
from tensorboard.plugins.hparams import api_pb2
from tensorboard.plugins.hparams import summary as hparams_summary
from google.protobuf import struct_pb2
import datetime


#custom library imports
from Networks.n_utils.dualpath_module import DualPathNetwork
from MoleculeCompletion.Utils.BasicPDBGenerator_crop import PDBStreamer

def main():
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='../sample_data/')
    parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 16)
    parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0001)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 256)
    parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = False)
    parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
    parser.add_argument('--crop', help="Whether or not to crop at fluorophore center, immediately implies check_size is False", default = False)
    parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 2.)
    parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
    parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
    
    # read in all arguments as dictionary "args"
    args = parser.parse_args()
    data_dir = args.data_directory
    key = args.key
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    n_aug = int(args.n_aug)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    aug = args.augment
    check_size = args.check_size
    crop = args.crop
    check_margin = args.margin
    vox_size = args.vox_size
    dims = args.dims
    dims = (dims,dims,dims)
    
    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    
        # this logic tries to detect a key in the pdb file, if one is not specified at the command line
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]

     #make a list of all the pdb files matching the wildcard
    data_list = glob.glob(data_dir)
    print("Found %i structures" %len(data_list))
    print("Using key: " + str(key))

    #reads in the key file and creates a dictionary
    y_dict = {}
    fluorophore_dict = {}
    with open(key, encoding="utf8", errors='ignore', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for rows in csv_reader:
            if crop:
                f_coords = []
                f_coords.append(float(rows[2]))
                f_coords.append(float(rows[3]))
                f_coords.append(float(rows[4]))
                fluorophore_dict[rows[0]] = [float(rows[1]), tuple(f_coords)]

            else:
                y_dict[rows[0]] = float(rows[1])

    # robust method to make 0 to 1 scaling, based on the maximum and minimum lambda emission values found in FPbase
    # The interval could be tweaked to be less biased to this database
    light_spectrum_nm = np.arange(355, 721).astype(float)
    scaler = MinMaxScaler(feature_range=(0, 1), copy=True)
    scaler.fit(light_spectrum_nm.reshape(-1,1))
    scaled_light_spectrum = scaler.transform(light_spectrum_nm.reshape(-1,1)).flatten()
    scaled_light_spectrum_dict = dict(zip(light_spectrum_nm, scaled_light_spectrum))

    if crop:
        for key in fluorophore_dict.keys():
            val = scaled_light_spectrum_dict[fluorophore_dict[key][0]]
            fluorophore_dict[key][0]= val
    else:
        for key in y_dict.keys():
            val = scaled_light_spectrum[y_dict[key]]
            y_dict[key]= val

    train_data_list = []

    for entries in data_list:
        base = os.path.basename(entries)
        structure_name = os.path.splitext(base)[0]
        
        # this is basically allowing there to be two different keys referring to the same file
        
        if crop:
            try:
                try:
                    y_value = fluorophore_dict[structure_name]
                    train_data_list.append([entries, y_value])
                except:
                    y_value = fluorophore_dict[structure_name.upper()]
                    train_data_list.append([entries, y_value])
            except:
                continue

        else:
            try:
                try:
                    y_value = y_dict[structure_name]
                    train_data_list.append([entries, y_value])
                except:
                    y_value = y_dict[structure_name.upper()]
                    train_data_list.append([entries, y_value])
            except:
                continue

    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))

    # https://arxiv.org/abs/1707.01629
    
    pdb_input = Input(shape=(32, 32, 32, 7), name='pdb_read_in')
    main_model = DualPathNetwork(pdb_input)
    
    # here we declare the model starts at "pdb_input" and ends and "N_out". Keras will link all of the layers in between. Note we can reuse layer names (like we did with "N"), but the input and output layer names must be unique
    adam = Adam(lr=learning_rate)

    main_model.compile(optimizer=adam, loss=['mse'])
    main_model.summary()

    #set random seed, this will allow the random splitting we do to be reproducible
    np.random.seed(42)

    # choose test data
    train_len = len(train_data_list)
    test_len = train_len // 4
    test_indices = np.random.choice(np.arange(train_len), test_len, replace=False)
    test_indices = sorted(test_indices, reverse=True)

    #make test data and remove from training
    test_data_list = []
    for i in test_indices:
        test_data_list.append(train_data_list.pop(i))

    #we make two copies of the PDBstreamer object, one which gets fed the training data list, the other gets the test data list
    train_streamer = PDBStreamer(train_data_list, n_augmentations = n_aug, batch_size=batch_size, augment = aug, dim = dims, voxel_size = vox_size, check_sizes = check_size, crops = crop)
    test_streamer = PDBStreamer(test_data_list, n_augmentations = n_aug, batch_size=batch_size, augment = aug, dim = dims, voxel_size = vox_size, check_sizes = check_size, crops = crop)
    
    #train the model:
    main_model.fit(train_streamer, validation_data = test_streamer, epochs=epochs, shuffle = True, workers = 1, max_queue_size = 10, use_multiprocessing=False)


if __name__ == "__main__":
    main()