#Import built in libraries
import glob, os, csv, random

#Third party import
import argparse
import numpy as np
import sklearn
from sklearn.preprocessing import MinMaxScaler

#Third party imports from keras
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv3D, MaxPool3D, BatchNormalization, Input, AveragePooling3D
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras import backend as K

#hyperparameter tuning
from tensorboard.plugins.hparams import api as hp
from tensorboard.plugins.hparams import api_pb2
from tensorboard.plugins.hparams import summary as hparams_summary
from google.protobuf import struct_pb2
import datetime


#custom library imports
from Networks.n_utils.dualpath_module import DualPathNetwork
from Networks.n_utils.custom_metrics import stdsq
from MoleculeCompletion.Utils.BasicPDBGenerator_crop import PDBStreamer

def train_test_model(run_dir, hparams):
    """
    Trains DPN92: --depth [3, 4, 20, 3] --filter_increment [16, 32, 24, 128]
    
    Trains DPN98: --depth [3, 6, 20, 3] --filter_increment [16, 32, 32, 128]
    
    Trains DPN131: --depth [4, 8, 28, 3] --filter_increment [16, 32, 32, 128]
    """
    
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='../sample_data/')
    parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 16)
    parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0001)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 256)
    parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = False)
    parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
    parser.add_argument('--crop', help="Whether or not to crop at fluorophore center, immediately implies check_size is False", default = False)
    parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 2.)
    parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
    parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
    parser.add_argument('--depth', type=list, help = 'numbrer of layers in each block', default=[3, 4, 20, 3])
    parser.add_argument('--filter_increment', type=list, help='number of filters incremented per block', default=[16, 32, 24, 128])
    parser.add_argument('--cardinality', type=int, help='the size of the set of transformations', default=32)
    parser.add_argument('--width', type=int, help='width multiplier for the network', default=3)
    
    # read in all arguments as dictionary "args"
    args = parser.parse_args()
    data_dir = args.data_directory
    key = args.key
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    n_aug = int(args.n_aug)
    #learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    #n_filters = int(args.n_filters)
    aug = args.augment
    check_size = args.check_size
    crop = args.crop
    check_margin = args.margin
    vox_size = args.vox_size
    dims = args.dims
    dims = (dims,dims,dims)
    depth = args.depth
    filter_increment = args.filter_increment
    #cardinality = int(args.cardinality)
    #width = int(args.width)
    
    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    
        # this logic tries to detect a key in the pdb file, if one is not specified at the command line
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]

     #make a list of all the pdb files matching the wildcard
    data_list = glob.glob(data_dir)
    print("Found %i structures" %len(data_list))
    print("Using key: " + str(key))

    #reads in the key file and creates a dictionary
    y_dict = {}
    fluorophore_dict = {}
    with open(key, encoding="utf8", errors='ignore', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for rows in csv_reader:
            if crop:
                f_coords = []
                f_coords.append(float(rows[2]))
                f_coords.append(float(rows[3]))
                f_coords.append(float(rows[4]))
                fluorophore_dict[rows[0]] = [float(rows[1]), tuple(f_coords)]

            else:
                y_dict[rows[0]] = float(rows[1])

    # robust method to make 0 to 1 scaling, based on the maximum and minimum lambda emission values found in FPbase
    # The interval could be tweaked to be less biased to this database
    light_spectrum_nm = np.arange(355, 721).astype(float)
    scaler = MinMaxScaler(feature_range=(0, 1), copy=True)
    scaler.fit(light_spectrum_nm.reshape(-1,1))
    scaled_light_spectrum = scaler.transform(light_spectrum_nm.reshape(-1,1)).flatten()
    scaled_light_spectrum_dict = dict(zip(light_spectrum_nm, scaled_light_spectrum))

    if crop:
        for key in fluorophore_dict.keys():
            val = scaled_light_spectrum_dict[fluorophore_dict[key][0]]
            fluorophore_dict[key][0]= val
    else:
        for key in y_dict.keys():
            val = scaled_light_spectrum[y_dict[key]]
            y_dict[key]= val

    train_data_list = []

    for entries in data_list:
        base = os.path.basename(entries)
        structure_name = os.path.splitext(base)[0]
        
        # this is basically allowing there to be two different keys referring to the same file
        
        if crop:
            try:
                try:
                    y_value = fluorophore_dict[structure_name]
                    train_data_list.append([entries, y_value])
                except:
                    y_value = fluorophore_dict[structure_name.upper()]
                    train_data_list.append([entries, y_value])
            except:
                continue

        else:
            try:
                try:
                    y_value = y_dict[structure_name]
                    train_data_list.append([entries, y_value])
                except:
                    y_value = y_dict[structure_name.upper()]
                    train_data_list.append([entries, y_value])
            except:
                continue

    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))

    # Callback methods 
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights=True)
    TB = TensorBoard(run_dir + "/keras")
    KC = hp.KerasCallback(run_dir, hparams)
    
    my_callbacks = [ES, TB, KC]
    # https://arxiv.org/abs/1707.01629
    # n_filters start at 64
    pdb_input = Input(shape=(32, 32, 32, 7), name='pdb_read_in')
    main_model = DualPathNetwork(pdb_input, 
                                 initial_conv_filters=hparams['n_filters'], 
                                 depth=depth, 
                                 filter_increment=filter_increment, 
                                 cardinality=hparams['cardinality'], 
                                 width=hparams['width'],
                                 base_filters=hparams['n_filters'] * 4,
                                 kernel_regularizer=l2,
                                 regularization_factor=hparams['regularization_factor'],
                                 BN_momentum=hparams['momentum'], 
                                 dropout_rate=hparams['dropout_rate']
                                )
    
    # here we declare the model starts at "pdb_input" and ends and "N_out". Keras will link all of the layers in between. Note we can reuse layer names (like we did with "N"), but the input and output layer names must be unique
    adam = Adam(lr=hparams['learning_rate'])

    main_model.compile(optimizer=adam, loss=['mse'], metrics=[stdsq, 'mean_squared_error'])
    main_model.summary()

    #set random seed, this will allow the random splitting we do to be reproducible
    np.random.seed(42)

    # choose test data
    train_len = len(train_data_list)
    test_len = train_len // 4
    test_indices = np.random.choice(np.arange(train_len), test_len, replace=False)
    test_indices = sorted(test_indices, reverse=True)
    
    #make test data and remove from training
    test_data_list = []
    for i in test_indices:
        test_data_list.append(train_data_list.pop(i))
    
    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_data_list))
    #we make two copies of the PDBstreamer object, one which gets fed the training data list, the other gets the test data list
    train_streamer = PDBStreamer(train_data_list, n_augmentations = n_aug, batch_size=batch_size, augment = aug, dim = dims, voxel_size = vox_size, check_sizes = check_size, crops = crop)
    test_streamer = PDBStreamer(test_data_list, n_augmentations = n_aug, batch_size=batch_size, augment = aug, dim = dims, voxel_size = vox_size, check_sizes = check_size, crops = crop)
    
    #train the model:
    main_model.fit(train_streamer, validation_data = test_streamer, epochs=epochs, shuffle = True, workers = 1, max_queue_size = 10, use_multiprocessing=False, callbacks=my_callbacks)
    
    scores = main_model.evaluate(test_streamer)
    return scores 
    
    
learning_rate_list = [0.001, 0.0005, 0.001, 0.00005]
n_filters_list = [128, 256]
dropout_rate_list = [0, 0.3, 0.5]
kernel_regularizer_rate_list = [0.01, 0.001, 0.005]
BN_momentum_list = [0.9, 0.99] 
width_list = [3, 5]
cardinality_list = [16, 32]

def create_experiment_summary(learning_rate_list, n_filters_list, dropout_rate_list, kernel_regularizer_rate_list, BN_momentum_list, width_list, cardinality_list):
    learning_rate_list = struct_pb2.ListValue()
    learning_rate_list.extend(learning_rate_list)
    
    n_filters_list = struct_pb2.ListValue()
    n_filters_list.extend(n_filters_list)
    
    dropout_rate_list = struct_pb2.ListValue()
    dropout_rate_list.extend(dropout_rate_list)
    
    kernel_regularizer_rate_list = struct_pb2.ListValue()
    kernel_regularizer_rate_list.extend(kernel_regularizer_rate_list)
    
    BN_momentum_list = struct_pb2.ListValue()
    BN_momentum_list.extend(BN_momentum_list)
    
    width_list = struct_pb2.ListValue()
    width_list.extend(width_list)
    
    cardinality_list = struct_pb2.ListValue()
    cardinality_list.extend(cardinality_list)
    
    return hparams_summary.experiment_pb(
        hparam_infos=[
            api_pb2.HParamInfo(name='learning_rate',
                               display_name='learning rate',
                               type=api_pb2.DATA_TYPE_FLOAT64,
                               domain_discrete=learning_rate_list),
            api_pb2.HParamInfo(name='n_filters',
                               display_name = 'number of filters',
                               type=api_pb2.DATA_TYPE_FLOAT64,
                               domain_discrete=n_filters_list),
            api_pb2.HParamInfo(name= 'dropout_rate', 
                               display_name = 'dropout rate',
                               type=api_pb2.DATA_TYPE_FLOAT64,
                               domain_discrete=dropout_rate_list),
            api_pb2.HParamInfo(name="kernel_regularizer_rate",
                               display_name='regularization factor',
                               type=api_pb2.DATA_TYPE_FLOAT64,
                               domain_discrete=kernel_regularizer_rate_list),
            api_pb2.HParamInfo(name='BN_momentum', 
                               display_name='BatchNormalization momentum', 
                               type=api_pb2.DATA_TYPE_FLOAT64,
                               domain_discrete=BN_momentum_list),
            api_pb2.HParamInfo(name='width_list', 
                               display_name= 'width multiplier', 
                               type=api_pb2.DATA_TYPE_FLOAT64,
                               domain_discrete=width_list),
            api_pb2.HParamInfo(name='cardinality', 
                               display_name='cardinality',
                               type=api_pb2.DATA_TYPE_FLOAT64,
                               domain_discrete=cardinality_list)
        ], 
        # list metrics
        metric_infos=[
            api_pb2.MetricInfo( 
                name=api_pb2.MetricName(tag='mse'),
                display_name='MSE'),
        ]
    )
exp_summary = create_experiment_summary(learning_rate_list, n_filters_list, dropout_rate_list, kernel_regularizer_rate_list, BN_momentum_list, width_list, cardinality_list)
root_logdir_writer = tf.summary.create_file_writer('logs/hparam_tuning')
with root_logdir_writer.as_default():
    tf.summary.experimental.write_raw_pb(tf.compat.v1.Event(summary=exp_summary).SerializeToString(), step=0)


def run(run_dir, hparams):
    writer = tf.summary.create_file_writer(run_dir)
    summary_start = hparams_summary.session_start_pb(hparams=hparams)
    with writer.as_default():
        tf.summary.experimental.write_raw_pb(tf.compat.v1.Event(summary=exp_summary).SerializeToString(), step=0)
        mse, stdsq, mean_squared_error = train_test_model(run_dir, hparams)
        
        tf.summary.scalar('mse', mse, step=0, description="The Mean Squared Error")
        tf.summary.scalar('stdsq', stdsq, step=0, description='The Variance')
        tf.summary.scalar('mean_squared_error', mean_squared_error, step=0, description='The Mean Squared Error Metric')
        summary_end = hparams_summary.session_end_pb(api_pb2.STATUS_SUCCESS)
        tf.summary.experimental.write_raw_pb(tf.compat.v1.Event(summary=exp_summary).SerializeToString(), step=0)
        
def main():
    session_num = 0
    
    for learning_rate in learning_rate_list:
        for n_filters in n_filters_list:
            for dropout_rate in dropout_rate_list:
                for regularization_factor in kernel_regularizer_rate_list:
                    for momentum in BN_momentum_list:
                        for width in width_list:
                            for cardinality in cardinality_list:
                                hparams = {
                                    'learning_rate': learning_rate,
                                    'n_filters': n_filters, 
                                    'dropout_rate': dropout_rate,
                                    'regularization_factor': regularization_factor,
                                    'momentum': momentum,
                                    'width': width,
                                    'cardinality': cardinality
                                }
                                label = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
                                run_name = "/run-{}/".format(label)
                                print('--- Starting trial: %s' % run_name)
                                print(hparams.items())
                                run('logs/hparam_tuning/' + run_name + 'DPN92', hparams)
                                session_num += 1
                                
                                tf.keras.backend.clear_session()

if __name__ == "__main__":
    main()