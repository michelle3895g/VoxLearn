# third party imports 
from tensorflow import keras
from tensorflow.keras.layers import Input, Dense, Conv3D, MaxPool3D
from tensorflow.keras.models import Model
from tensorflow.keras.layers import concatenate
from tensorflow.keras.regularizers import l2

"""
All inception blocks for inception CNN's
"""


def inception_AJA_3(input, filters_1x1x1, filters_3x3x3_reduce, filters_1x1x3, filters_1x3x1, filters_3x1x1, filters_pool_proj, kernel_regularizer, regularization_factor):
    """
    inception module to reduce # of parameters in AJA_template
    
    params
    :input: (keras tensor) input tensor
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x3: (int or 3 element tuple) number of filters for convolution
    :filters_1x3x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
    :kernel_regularizer: (string) `l2` or `l1` expected
    :regularization_factor: (int) regularization factor for regularization penalty
    
    Returns: keras tensor
    """
    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(input)

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=kernel_regularizer(regularization_factor))(input)
    conv_1x1x3 = Conv3D(filters=filters_1x1x3, 
                        kernel_size=(1,1,3), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(conv_3x3x3_reduce)
    conv_1x3x1 = Conv3D(filters=filters_1x3x1, 
                        kernel_size=(1,3,1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x1x3)
    conv_3x1x1 = Conv3D(filters=filters_3x1x1, 
                        kernel_size=(3,1,1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x3x1)
    
    maxpool = MaxPool3D(pool_size=3, 
                        strides=1, 
                        padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, 
                          kernel_size=1, 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=kernel_regularizer(regularization_factor))(maxpool)
    
    inception_output = concatenate([conv_1x1x1, conv_1x1x3, conv_1x3x1, conv_3x1x1, maxpool_proj], axis=-1)
    return inception_output 

def inception_AJA_6(input, filters_1x1x1, filters_6x6x6_reduce, filters_1x1x6, filters_1x6x1, filters_6x1x1, filters_pool_proj, kernel_regularizer, regularization_factor):
    """
    inception module to reduce # of parameters in AJA_template
    
    params
    :input: (keras tensor) input tensor
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_6x6x6_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x6: (int or 3 element tuple) number of filters for convolution
    :filters_1x6x1: (int or 3 element tuple) number of filters for convolution
    :filters_6x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
    :kernel_regularizer: (string) `l2` or `l1` expected
    :regularization_factor: (int) regularization factor for regularization penalty
    
    Returns: keras tensor
    """
    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(input)

    conv_6x6x6_reduce = Conv3D(filters=filters_6x6x6_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=kernel_regularizer(regularization_factor))(input)
    conv_1x1x6 = Conv3D(filters=filters_1x1x6, 
                        kernel_size=(1,1,6), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(conv_6x6x6_reduce)
    conv_1x6x1 = Conv3D(filters=filters_1x6x1, 
                        kernel_size=(1,6,1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x1x6)
    conv_6x1x1 = Conv3D(filters=filters_6x1x1, 
                        kernel_size=(6,1,1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x6x1)
    
    maxpool = MaxPool3D(pool_size=3, strides=1, padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, kernel_size=1, padding='same', activation='relu',
                          kernel_regularizer=kernel_regularizer(regularization_factor))(maxpool)
    
    inception_output = concatenate([conv_1x1x1, conv_1x1x6, conv_1x6x1, conv_6x1x1, maxpool_proj], axis=-1)
    return inception_output 

def inception_v1(input, filters_1x1x1, filters_3x3x3_reduce, filters_3x3x3, filters_5x5x5_reduce, filters_5x5x5, filters_pool_proj, kernel_regularizer, regularization_factor):
    """
    keras implemented inception module based on 2DgoogLeNet, https://github.com/dingchenwei/googLeNet/blob/master/inceptionModel.py
    
    params
    :input: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3: (int or 3 element tuple) number of filters for convolution
    :filters_5x5x5_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_5x5x5: (int or 3 element tuple) number of filters for convolution
    :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
    :kernel_regularizer: (string) `l2` or `l1` expected
    :regularization_factor: (int) regularization factor for regularization penalty
    
    Returns: keras tensor
    """
    
    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(input)

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=kernel_regularizer(regularization_factor))(input)
    conv_3x3x3 = Conv3D(filters=filters_3x3x3, 
                        kernel_size=3, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor)) (conv_3x3x3_reduce)

    conv_5x5x5_reduce = Conv3D(filters=filters_5x5x5_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=kernel_regularizer(regularization_factor))(input)
    conv_5x5x5 = Conv3D(filters=filters_5x5x5, 
                        kernel_size=5, padding='same', 
                        activation='relu',
                        kernel_regularizer=kernel_regularizer(regularization_factor))(conv_5x5x5_reduce)

    maxpool = MaxPool3D(pool_size=3, 
                        strides=1, 
                        padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, 
                          kernel_size=1, 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=kernel_regularizer(regularization_factor))(maxpool)
    
    inception_output = concatenate([conv_1x1x1, conv_3x3x3, conv_5x5x5, maxpool_proj], axis = -1)
    return inception_output

# inception module with factorizing convolutions (Szegedy 2016), reduce the number of connections/ parameters w/o decreasing network efficiency
def inception_v3_a(input, filters_1x1x1, filters_3x3x3_reduce, filters_3x3x3, filters_pool_proj):
    """
    inception module figure 5 https://arxiv.org/pdf/1512.00567v2.pdf
    
    params
    :input: (keras tensor) input tensor
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x3: (int or 3 element tuple) number of filters for convolution
    :filters_1x3x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
    
    Returns: keras tensor
    """
    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu', 
                               kernel_regularizer=l2(0.01))(input)
    conv_3x3x3_1 = Conv3D(filters=filters_3x3x3, 
                          kernel_size=3, 
                          padding='same', 
                          activation='relu', 
                          kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)
    conv_3x3x3_2 = Conv3D(filters=filters_3x3x3, 
                          kernel_size=3, 
                          padding='same', 
                          activation='relu', 
                          kernel_regularizer=l2(0.01))(conv_3x3x3_1)

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu', 
                               kernel_regularizer=l2(0.01))(input)
    conv_3x3x3_3 = Conv3D(filters=filters_3x3x3, 
                          kernel_size=3, 
                          padding='same', 
                          activation='relu', 
                          kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)

    maxpool = MaxPool3D(pool_size=3, 
                        strides=1, 
                        padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, 
                          kernel_size=1, 
                          padding='same', 
                          activation='relu', 
                          kernel_regularizer=l2(0.01))(maxpool)

    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, 
                        padding='same', 
                        activation='relu', 
                        kernel_regularizer=l2(0.01))(input)
    inception_output = concatenate([conv_3x3x3_1, conv_3x3x3_2, conv_3x3x3_3, maxpool_proj, conv_1x1x1], axis=-1) # don't know what axis
    return inception_output

def inception_v3_b(input, filters_1x1x1, filters_3x3x3_reduce, filters_1x1x3, filters_1x3x1, filters_3x1x1, filters_pool_proj):
    """
    inception module figure 6 https://arxiv.org/pdf/1512.00567v2.pdf
    
    params
    :input: (keras tensor) input tensor
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x3: (int or 3 element tuple) number of filters for convolution
    :filters_1x3x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
    
    Returns: keras tensor
    """
    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                        kernel_regularizer=l2(0.01))(input)
    conv_1x1x3 = Conv3D(filters=filters_1x1x3, 
                        kernel_size=(1,1,3), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)
    conv_1x3x1 = Conv3D(filters=filters_1x3x1, 
                        kernel_size=(1,3,1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_1x1x3)
    conv_3x1x1 = Conv3D(filters=filters_3x1x1, 
                        kernel_size=(3,1,1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))( conv_1x3x1)
    conv_3x1x1_1 = Conv3D(filters=filters_3x1x1, 
                          kernel_size=(3, 1, 1), 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x1x1)
    conv_1x3x1_1 = Conv3D(filters=filters_1x3x1, 
                          kernel_size=(1, 3, 1), 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x1x1_1)
    conv_1x1x3_1 = Conv3D(filters=filters_1x1x3, 
                          kernel_size=(1, 1, 3), 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_1x3x1_1)

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=l2(0.01))(input)
    conv_1x1x3_2 = Conv3D(filters=filters_1x1x3, 
                          kernel_size=(1, 1, 3), 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)
    conv_1x3x1_2 = Conv3D(filters=filters_1x3x1, 
                          kernel_size=(1, 3, 1), 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_1x1x3_2)
    conv_3x1x1_2 = Conv3D(filters=filters_3x1x1, 
                          kernel_size=(3, 1, 1), 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_1x3x1_2)

    maxpool = MaxPool3D(pool_size=3, 
                        strides=1, 
                        padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, 
                          kernel_size=1, 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=l2(0.01))(maxpool)

    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(input)
    
    inception_output = concatenate([conv_1x1x3, conv_1x3x1, conv_3x1x1, conv_1x1x3_1, conv_1x3x1_1, conv_3x1x1_1, conv_1x1x3_2, conv_1x3x1_2, conv_3x1x1_2, maxpool_proj, conv_1x1x1], axis=-1)
    return inception_output

def inception_v3_c(input, filters_1x1x1, filters_3x3x3_reduce, filters_1x1x3, filters_1x3x1, filters_3x1x1, filters_3x3x3, filters_pool_proj):
    """
    inception module figure 7 https://arxiv.org/pdf/1512.00567v2.pdf
    
    :input: (keras tensor) input tensor
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x3: (int or 3 element tuple) number of filters for convolution
    :filters_1x3x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3: (int or 3 element tuple) number of filters for convolution
    :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
    
    Returns: keras tensor
    """

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=l2(0.01))(input)
    conv_3x3x3 = Conv3D(filters=filters_3x3x3, 
                        kernel_size=(3, 3, 3), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)
    conv_1x1x3 = Conv3D(filters=filters_1x1x3, kernel_size=(1, 1, 3), 
                        padding='same', activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3)
    conv_1x3x1 = Conv3D(filters=filters_1x3x1, 
                        kernel_size=(1, 3, 1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_1x1x3)
    conv_3x1x1 = Conv3D(filters=filters_3x1x1, 
                        kernel_size=(3, 1, 1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_1x3x1)

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=l2(0.01))(input)
    conv_1x1x3_1 = Conv3D(filters=filters_1x1x3, 
                          kernel_size=(1, 1, 3), 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)
    conv_1x3x1_1 = Conv3D(filters=filters_1x3x1, 
                          kernel_size=(1, 3, 1), 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=l2(0.01))(conv_1x1x3_1)
    conv_3x1x1_1 = Conv3D(filters=filters_3x1x1, 
                          kernel_size=(3, 1, 1), 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=l2(0.01))(conv_1x3x1_1)

    maxpool = MaxPool3D(pool_size=3, 
                        strides=1, 
                        padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, 
                          kernel_size=1, 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=l2(0.01))(maxpool)

    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(input)
    inception_output = concatenate([conv_3x3x3, conv_1x1x3, conv_1x3x1, conv_3x1x1, conv_1x1x3_1, conv_1x3x1_1, conv_3x1x1_1, conv_1x1x1,
         maxpool_proj, conv_1x1x1], axis=-1)
    return inception_output

# inception module from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5690636/

def inception1_3D(input, filters_1x1x1, filters_3x3x3_reduce, filters_3x3x3):
    """
    inception module 1 from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5690636/
    
    params
    :input: (keras tensor) input tensor
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3: (int or 3 element tuple) number of filters for convolution
    
    Returns: keras tensor
    """
    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(input)

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=l2(0.01))(input)
    conv_3x3x3 = Conv3D(filters=filters_3x3x3, 
                        kernel_size=3, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)

    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=l2(0.01))(input)
    conv_3x3x3_1 = Conv3D(filters=filters_3x3x3, 
                          kernel_size=3, 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)
    conv_3x3x3_2 = Conv3D(filters=filters_3x3x3, 
                          kernel_size=3, 
                          padding='same', 
                          activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3_1)

    maxpool = MaxPool3D(pool_size=3, 
                        strides=1, 
                        padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, 
                          kernel_size=1, 
                          padding='same', 
                          activation='relu',
                          kernel_regularizer=l2(0.01))(maxpool)
    
    inception_output = concatenate([conv_1x1x1, conv_3x3x3, conv_3x3x3_1, conv_3x3x3_2, maxpool_proj], axis=1)
    return inception_output

# inception module from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5690636/
def inception2_3D(input, filters_1x1x1, 
                  filters_3x3x3_reduce, filters_1x1x3, filters_1x3x1, filters_3x1x1,
                  filters_5x5x5_reduce,filters_1x1x5, filters_1x5x1, filters_5x1x1,
                  filters_pool_proj):
    """
    inception module 2 from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5690636/
    
    params
    :input: (keras tensor) input tensor
    :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x3: (int or 3 element tuple) number of filters for convolution
    :filters_1x3x1: (int or 3 element tuple) number of filters for convolution
    :filters_3x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_5x5x5_reduce: (int or 3 element tuple) number of filters for convolution
    :filters_1x1x5: (int or 3 element tuple) number of filters for convolution
    :filters_1x5x1: (int or 3 element tuple) number of filters for convolution
    :filters_5x1x1: (int or 3 element tuple) number of filters for convolution
    :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
    
    Returns: keras tensor
    """
    
    conv_1x1x1 = Conv3D(filters=filters_1x1x1, 
                        kernel_size=1, 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(input)
    conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu',
                               kernel_regularizer=l2(0.01))(input)
    conv_1x1x3 = Conv3D(filters=filters_1x1x3, 
                        kernel_size=(1,1,3), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_3x3x3_reduce)
    conv_1x3x1 = Conv3D(filters=filters_1x3x1, 
                        kernel_size=(1,3,1), 
                        padding='same', 
                        activation='relu',
                        kernel_regularizer=l2(0.01))(conv_1x1x3)
    conv_3x1x1 = Conv3D(filters=filters_3x1x1, 
                        kernel_size=(3,1,1), 
                        padding='same', 
                        activation='relu', 
                        kernel_regularizer=l2(0.01))(conv_1x3x1)

    conv_5x5x5_reduce = Conv3D(filters=filters_5x5x5_reduce, 
                               kernel_size=1, 
                               padding='same', 
                               activation='relu', 
                               kernel_regularizer=l2(0.01))(input)

    conv_1x1x5 = Conv3D(filters=filters_1x1x5, 
                        kernel_size=(1,1,5), 
                        padding='same', 
                        activation='relu', 
                        kernel_regularizer=l2(0.01))(conv_5x5x5_reduce)
    conv_1x5x1 = Conv3D(filters=filters_1x5x1, 
                        kernel_size=(1,5,1), 
                        padding='same', 
                        activation='relu', 
                        kernel_regularizer=l2(0.01))(conv_1x1x5)
    conv_5x1x1 = Conv3D(filters=filters_5x1x1, 
                        kernel_size=(5,1,1), 
                        padding='same', 
                        activation='relu', 
                        kernel_regularizer=l2(0.01))(conv_1x5x1)

    maxpool = MaxPool3D(pool_size=3, 
                        strides=1, 
                        padding='same')(input)
    maxpool_proj = Conv3D(filters=filters_pool_proj, 
                          kernel_size=1, 
                          padding='same', 
                          activation='relu', 
                          kernel_regularizer=l2(0.01))(maxpool)
    
    inception_ouput = concatenate([conv_1x1x1, conv_1x1x3, conv_1x3x1, conv_3x1x1, conv_1x1x5, conv_1x5x1, conv_5x1x1, maxpool_proj], axis=1)
    return inception_output