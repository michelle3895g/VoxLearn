import tensorflow as tf
import tensorflow_probability as tfp

"""
class stdsq(tf.keras.metrics.Metric):
    def __init__(self, name='stdsq', **kwargs):
        super(stdsq, self).__init__(name=name, **kwargs)
        self.stdsq = self.add_weight(name='tp')

    def update_state(self, y_true, y_pred, sample_weight=None):
        if sample_weight is None:
            print('sample_weight is None')
            y_true = tf.cast(y_true, tf.dtype)
            y_pred = tf.cast(y_pred, tf.dtype)

            difference = (y_true-y_pred)

            values = tfp.stats.variance(difference)
            print('sample_weight is none')
            self.stdsq.assign_add(values)

        else:
            sample_weight = tf.cast(sample_weight, self.dtype)
            sample_weight = tf.braodcast_weights(sample_weight, values)
            y_true = tf.cast(y_true, tf.float16)
            y_pred = tf.cast(y_pred, tf.float16)
            values_var = tf.reduce_sum((y_true - y_pred), 0, keepdims=True)
            values_var = tf.multiply(values_var, sample_weight)
            self.stdsq.assign_add(values_var)
            print('sample_weight given')

    def result(self):
        return self.stdsq
"""

def stdsq(y_true, y_pred): 
    difference = (y_true-y_pred)
    values = tfp.stats.variance(difference)
    return values