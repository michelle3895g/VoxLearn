import numpy as np 

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv3D, MaxPool3D, BatchNormalization, Input, AveragePooling3D, Lambda, ReLU, Add
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras import backend as K
from kerastuner import HyperModel

from Networks.n_utils.custom_metrics import stdsq

#There are two HyperModel types, but both can be used! I fixed one addition operation here, which connected the network. 

class ResNet(HyperModel):
    def __init__(self, name, block_pattern):
        self.name = name
        self.block_pattern = block_pattern

    def build(self, hp):
        
        n_filters = hp.Choice(name='n_filters', values=[64, 80, 96])
        BN_momentum = hp.Float('momentum', 0.9, 1.0, step=0.02)

        inputs = Input(shape=(32, 32, 32, 7), name='pdb_read_in')
        N = inputs

        N = Conv3D(kernel_size=7, strides=2, filters=n_filters, padding='same', data_format="channels_last")(N)
        N = BatchNormalization(momentum=BN_momentum)(N)
        N = Activation('relu')(N)

        N = MaxPool3D(pool_size=3, strides=2, padding='same')(N)

        for i in range(len(self.block_pattern)):
            num_blocks = self.block_pattern[i]
            for j in range(num_blocks):
                downsample = (j == 0 and i != 0)

                # lambda layer doesn't perform any function, it is used to call for addition.
                N_before = Lambda(lambda x: x*1)(N)

                # start of residual block
                N = Conv3D(kernel_size=3, strides=(1 if not downsample else 2), filters=n_filters,
                           padding='same')(N_before)
                N = ReLU()(N)
                N = BatchNormalization(momentum=BN_momentum)(N)
                N_2 = Conv3D(kernel_size=3, strides=1, filters=n_filters, padding='same')(N)

                if downsample:
                    N_3 = Conv3D(kernel_size=1, strides=2, filters=n_filters, padding='same')(N_before)
                    N = Add()([N_3, N_2])
                    N = ReLU()(N)
                    N = BatchNormalization(momentum=BN_momentum)(N)
                else:
                    N = Add()([N_before, N_2])
                    N = ReLU()(N)
                    N = BatchNormalization(momentum=BN_momentum)(N)

            n_filters *= 2

        N = AveragePooling3D(padding='same')(N)
        N = Flatten()(N)
        N = Dropout(hp.Choice(name='dropout_rate', values=[0.0, 0.1, 0.3, 0.5]))(N)
        outputs = Dense(1, activation='linear', name='Output')(N)

        model = keras.Model(inputs, outputs)
        adam = Adam(lr=hp.Choice(name='learning_rate', values=[0.0001, 0.0005]))

        model.compile(optimizer=adam, loss=['mse'], metrics=[stdsq, 'mean_squared_error'])
        model.summary()

        return model
