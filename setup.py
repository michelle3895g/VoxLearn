#!/home/aiden/anaconda3/bin/python

from setuptools import setup, find_packages
setup(
    name="MoleculeCompletion",
    version="0.1",
    author = "Aiden Aceves",
    author_email = "ajaceves@gmail.com",
    packages=find_packages(),
)
